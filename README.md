# StatusOK

### Config example

config.yaml
```yaml
interval: 5                 # Optional, default is 56
web:
  host: 0.0.0.0             # Optional, default is ''
  port: 8080                # Optional, default is 8080
  prefix: /prefix           # Optional, default is ''
checker:
  dumpPath: last-state.json # Optional, default is 'last-state.json'
hosts:
  - url: https://shub.in
    status: 200             # Optional, default is 200
  - url: https://kristina.shub.in/404
```

### Build & Run

```bash
git clone git@gitlab.com:shub.in/statusok.git
cd statusok
go get
go build cmd/statusok.go
./statusok -config=config.yaml
```