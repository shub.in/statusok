package main

import (
	"flag"
	"gitlab.com/shub.in/statusok/internal/checker"
	"gitlab.com/shub.in/statusok/internal/model"
	"gitlab.com/shub.in/statusok/internal/notifier"
	"gitlab.com/shub.in/statusok/internal/web"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	configPath := flag.String("config", "config.yaml", "Path to config file")
	flag.Parse()

	conf := model.ReadConfig(*configPath)
	store := model.NewStore()
	chckr := checker.NewChecker(conf, store)
	notif := notifier.NewNotifier(conf, chckr.GetUpdateCh())
	srv := web.NewWebServer(conf, store, notif)

	go chckr.Start()
	go srv.Start()
	go notif.Run()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	notif.Stop()
	chckr.Stop()
	srv.Stop()

	log.Printf("Exit")
}
