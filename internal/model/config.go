package model

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type WebConfig struct {
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
	Prefix string `yaml:"prefix"`
}

func (c WebConfig) Addr() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

func (c *WebConfig) URL() string {
	host := c.Host
	if host == "" {
		host = "127.0.0.1"
	}
	return fmt.Sprintf("http://%s:%d%s", host, c.Port, c.Prefix)
}

type CheckerConfig struct {
	DumpPath string `yaml:"dumpPath"`
}

type Config struct {
	Interval int           `yaml:"interval"`
	Web      WebConfig     `yaml:"web"`
	Hosts    []Host        `yaml:"hosts"`
	Checker  CheckerConfig `yaml:"checker"`
}

func NewConfig() *Config {
	return &Config{
		Web: WebConfig{
			Port:   8080,
			Prefix: "",
		},
		Interval: 5,
		Checker:  CheckerConfig{DumpPath: "last-state.json"},
	}
}

func ReadConfig(path string) *Config {
	log.Println("Reading config...")
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Error while %s", err.Error())
	}
	config := NewConfig()
	err = yaml.Unmarshal(data, config)
	if err != nil {
		log.Printf("Error while parsing %s", err.Error())
	}

	// Exclude hosts with empty URL, set status as 200 if not exists
	tmp := config.Hosts[:0]
	for _, host := range config.Hosts {
		if host.Url != "" {
			if host.Status == 0 {
				host.Status = 200
			}
			tmp = append(tmp, host)
		}
	}
	config.Hosts = tmp
	return config
}
