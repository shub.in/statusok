package model

import (
	"log"
	"time"
)

type Status struct {
	Text      string    `json:"text"`
	Alive     bool      `json:"is_alive"`
	ChangedAt time.Time `json:"changed_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Duration  int64     `json:"duration"`
}

type Store struct {
	Statuses map[string]Status
}

func NewStore() *Store {
	log.Println("Initialize store...")
	return &Store{Statuses: make(map[string]Status)}
}
