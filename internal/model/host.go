package model

import (
	"net/http"
	"time"
)

type Host struct {
	Url    string `yaml:"url"`
	Status int    `yaml:"status"`
}

func (host *Host) Check() (bool, string, int64) {
	client := http.Client{Timeout: 1 * time.Second}
	start := time.Now()
	resp, err := client.Get(host.Url)
	if err != nil {
		return false, "600 ERROR", time.Since(start).Milliseconds()
	}
	return resp.StatusCode == host.Status, resp.Status, time.Since(start).Milliseconds()
}
