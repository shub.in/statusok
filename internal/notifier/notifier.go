package notifier

import (
	"gitlab.com/shub.in/statusok/internal/model"
	"log"
	"sync"
)

type Notifier struct {
	config   *model.Config
	updateCh <-chan map[string]model.Status
	closeCh  chan bool
	subs     map[chan map[string]model.Status]bool
	mutex    *sync.RWMutex
}

func NewNotifier(config *model.Config, updateCh <-chan map[string]model.Status) *Notifier {
	log.Println("Initialize notifier...")
	return &Notifier{
		config:   config,
		updateCh: updateCh,
		closeCh:  make(chan bool),
		subs:     make(map[chan map[string]model.Status]bool),
		mutex:    &sync.RWMutex{},
	}
}

func (n Notifier) Subscribe() chan map[string]model.Status {
	log.Println("New subscriber!")
	ch := make(chan map[string]model.Status)
	n.mutex.Lock()
	n.subs[ch] = true
	n.mutex.Unlock()
	return ch
}

func (n Notifier) Unsibscribe(ch chan map[string]model.Status) {
	log.Println("Subscriber leave")
	n.mutex.Lock()
	delete(n.subs, ch)
	n.mutex.Unlock()
}

func (n Notifier) Run() {
	log.Println("Run notifier")
	for {
		select {
		case update := <-n.updateCh:
			n.mutex.RLock()
			for ch, _ := range n.subs {
				ch <- update
			}
			n.mutex.RUnlock()
		case <-n.closeCh:
			break
		}
	}
}

func (n Notifier) Stop() {
	log.Println("Stop notifier")
	n.mutex.Lock()
	for ch, _ := range n.subs {
		close(ch)
		delete(n.subs, ch)
	}
	n.mutex.Unlock()
	n.closeCh <- true
}
