package web

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/shub.in/statusok/internal/model"
	"gitlab.com/shub.in/statusok/internal/notifier"
	"io"
	"net/http"
)

func redirectToHomeHandler(config *model.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Redirect(http.StatusTemporaryRedirect, config.Web.Prefix)
	}
}

func homeHandler(config *model.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", config.Web)
	}
}

func statHandler(store *model.Store) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(http.StatusOK, store.Statuses)
	}
}

func sseHandler(notifier *notifier.Notifier) gin.HandlerFunc {
	return func(c *gin.Context) {
		ch := notifier.Subscribe()
		defer func() {
			notifier.Unsibscribe(ch)
		}()

		c.Stream(func(w io.Writer) bool {
			select {
			case data, ok := <-ch:
				if !ok {
					c.SSEvent("close", nil)
					return false
				}
				c.SSEvent("", data)
			}
			return true
		})
	}
}
