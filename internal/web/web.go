package web

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/shub.in/statusok/internal/model"
	"gitlab.com/shub.in/statusok/internal/notifier"
	"log"
	"net/http"
	"time"
)

type WebServer struct {
	srv    http.Server
	config model.WebConfig
}

func (web *WebServer) Start() {
	log.Printf("Run web server. Stats here %s", web.config.URL())
	if err := web.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Listen: %s", err)
	}
}

func (web *WebServer) Stop() {
	log.Println("Stop web server")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := web.srv.Shutdown(ctx); err != nil {
		log.Fatal("Web server forced to shutdown: ", err)
	}
}

func NewWebServer(config *model.Config, store *model.Store, notifier *notifier.Notifier) *WebServer {
	log.Println("Initialize web server...")
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")

	if config.Web.Prefix != "" && config.Web.Prefix != "/" {
		router.GET("", redirectToHomeHandler(config))
	}

	group := router.Group(config.Web.Prefix)
	{
		group.GET("", homeHandler(config))
		group.GET("/stat", statHandler(store))
		group.GET("/sse", sseHandler(notifier))
	}

	server := &WebServer{
		srv: http.Server{
			Addr:    config.Web.Addr(),
			Handler: router,
		},
		config: config.Web,
	}
	return server
}
