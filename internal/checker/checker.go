package checker

import (
	"encoding/json"
	"gitlab.com/shub.in/statusok/internal/model"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type Checker struct {
	config   *model.Config
	store    *model.Store
	closeCh  chan bool
	ticker   *time.Ticker
	needDump bool
	updateCh chan map[string]model.Status
}

func NewChecker(config *model.Config, store *model.Store) *Checker {
	log.Println("Initialize checker...")
	return &Checker{
		config:   config,
		store:    store,
		closeCh:  make(chan bool),
		ticker:   time.NewTicker(time.Duration(config.Interval) * time.Second),
		needDump: true,
		updateCh: make(chan map[string]model.Status),
	}
}

func (checker *Checker) CheckHosts() {
	for _, host := range checker.config.Hosts {
		alive, text, duration := host.Check()
		status, exists := checker.store.Statuses[host.Url]
		if !exists {
			status = model.Status{
				Text:      text,
				Alive:     alive,
				ChangedAt: time.Now(),
				UpdatedAt: time.Now(),
				Duration:  duration,
			}
		} else {
			status.UpdatedAt = time.Now()
			status.Text = text
			status.Duration = duration
			if alive != status.Alive {
				status.Alive = alive
				status.ChangedAt = time.Now()
			}
		}
		checker.store.Statuses[host.Url] = status
	}
	checker.updateCh <- checker.store.Statuses
	if checker.needDump {
		checker.dumpStatuses()
	}
}

func (checker *Checker) Start() {
	checker.loadStatuses()
	log.Println("Run checker")
	checker.CheckHosts()
	for {
		select {
		case <-checker.closeCh:
			log.Println("Stop checker")
			return
		case <-checker.ticker.C:
			checker.CheckHosts()
		}
	}
}

func (checker *Checker) Stop() {
	checker.closeCh <- true
}

func (checker *Checker) dumpStatuses() {
	data, err := json.Marshal(checker.store.Statuses)
	if err != nil {
		log.Printf("Error while dump statuses [json]: %s", err)
		checker.needDump = false
	}
	err = ioutil.WriteFile(checker.config.Checker.DumpPath, data, 0644)
	if err != nil {
		log.Printf("Error while dump statuses [file]: %s", err)
		checker.needDump = false
	}
}

func (checker *Checker) loadStatuses() {
	data, err := ioutil.ReadFile(checker.config.Checker.DumpPath)
	if err != nil && !os.IsNotExist(err) {
		log.Printf("Error while load statuses [file]: %s", err)
	} else if os.IsNotExist(err) {
		return
	}
	err = json.Unmarshal(data, &checker.store.Statuses)
	if err != nil {
		log.Printf("Error while load statuses [json]: %s", err)
	}
}

func (checker *Checker) GetUpdateCh() <-chan map[string]model.Status {
	return checker.updateCh
}
